#!/usr/bin/env python
# coding: utf-8

# In[20]:


import pandas as pd
from google.cloud import storage
client = storage.Client()
import numpy as np


# In[21]:


data=pd.read_csv('gs://mmm-ford-de-bucket/input/Ford_real_data/Real Data.csv')


# In[22]:


data.head()


# In[300]:


Month={'ENERO':'Jan','FEBRERO':'Feb','MARZO':'Mar','ABRIL':'Apr','MAYO':'May','JUNIO':'Jun','JULIO':'Jul','AGOSTO':'Aug','SEPTIEMBRE':'Sep','OCTUBRE':'Oct','NOVIEMBRE':'Nov','DICIEMBRE':'Dec'}


# In[301]:


""""dataframe contains manfacturer, tier and channel"""
# data1=data.iloc[:,:3]
# data1.rename(columns=data1.iloc[1], inplace = True)
# data1 = data1.drop(labels=[0,1], axis=0)
# data1.reset_index(drop='index')
# index_names = data1[ (data1['Manufacturer'] == 'Manufacturer')].index
# data1.drop(index_names, inplace = True)


# In[302]:


"""dataframe contains investment"""
# data2=data.iloc[:,3:15]
# data2.rename(columns=data2.iloc[1], inplace = True)
# data2 = data2.drop(labels=[0,1], axis=0)
# data2=data2.transpose()
# data2 = data2.rename_axis('Months')
# data2=data2.reset_index()
# data2['Months']=data2['Months'].map(Month)   


# In[303]:


"""dataframe contains insertion"""
# data3 = data.iloc[:,15:]
# data3.rename(columns=data3.iloc[1], inplace = True)
# data3 = data3.drop(labels=[0,1], axis=0)


# In[536]:


"""investment data"""
data_inv=data.iloc[:,:15]
data_inv.rename(columns=data_inv.iloc[1], inplace = True)
# data_inv = data_inv.drop(labels=[0], axis=0)
data_inv=pd.melt(data_inv,id_vars=['Manufacturer','Tier','Channel'],var_name='Months',value_name='Investment')
# data_inv["year"]=data_inv.apply(lambda x: x['Investment'] if pd.isna(x["Manufacturer"]) else None,axis=1)


# In[322]:


data_inv=data.iloc[:,:15]


# In[323]:


data_inv.rename(columns=data_inv.iloc[1], inplace = True)


# In[324]:


data_inv.head()


# In[ ]:





# In[ ]:





# In[ ]:





# In[537]:


data_inv["year"]=data_inv.apply(lambda x: x['Investment'] if pd.isna(x["Manufacturer"]) else None,axis=1)


# In[539]:


data_inv['year']=data_inv['year'].fillna(method="ffill")
data_inv=data_inv[~data_inv["Manufacturer"].isin([np.nan,"Manufacturer"])]       



# In[ ]:


###########################################################


# In[3]:


"""investment"""
data_inv=data.iloc[:,:15]
data_inv.rename(columns=data_inv.iloc[1], inplace = True)
data_inv=pd.melt(data_inv,id_vars=['Manufacturer','Tier','Channel'],var_name='Months',value_name='Investment')
data_inv["year"]=data_inv.apply(lambda x: x['Investment'] if pd.isna(x["Manufacturer"]) else None,axis=1)
data_inv['year']=data_inv['year'].fillna(method="ffill")
data_inv=data_inv[~data_inv["Manufacturer"].isin([np.nan,"Manufacturer"])] 


# In[4]:


data_inv


# In[ ]:





# In[ ]:





# In[ ]:





# In[5]:


data_inv["year"]=data_inv.apply(lambda x: x['Investment'] if pd.isna(x["Manufacturer"]) else None,axis=1)


# In[6]:


data_inv.head()


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[23]:


"""insertion"""
data_ins=data.drop(data.iloc[:,3:15],axis=1)
data_ins.rename(columns=data_ins.iloc[1], inplace = True)
data_ins=pd.melt(data_ins,id_vars=['Manufacturer','Tier','Channel'],var_name='Months',value_name='Insertion')
data_ins["year"]=data_ins.apply(lambda x: x['Insertion'] if pd.isna(x["Manufacturer"]) else None,axis=1)
data_ins['year']=data_ins['year'].fillna(method="ffill")
data_ins=data_ins[~data_ins["Manufacturer"].isin([np.nan,"Manufacturer"])] 


# In[24]:


'''extracting year'''
year=data_ins['year'].unique().tolist()
yr=[]
for i in year:
    yr.append([i]*16)
year_pd=pd.DataFrame(yr)
year_pd = pd.DataFrame(year_pd.values.ravel(), columns=['year']) 


# In[25]:


"""investment updated"""
data_inv=data.iloc[:,:15]
data_inv.rename(columns=data_inv.iloc[1], inplace = True)


# In[26]:


update_inv = data_inv.drop([data_inv.index[0], data_inv.index[1]])
update_inv=update_inv.reset_index(drop=True)
update_inv = update_inv.dropna(subset=['Manufacturer'])


# In[27]:


# update_inv = update_inv[update_inv['Manufacturer'].notna()]
update_inv.drop(update_inv.loc[update_inv['Manufacturer']=='Manufacturer'].index, inplace=True)


# In[28]:


# result = pd.concat([update_inv, year_pd])
result_inv=pd.merge(update_inv, year_pd, left_index=True, right_index=True)


# In[29]:


result_inv=pd.melt(result_inv, id_vars =['Manufacturer','Tier','Channel','year'],var_name='Months',value_name='Investment')


# In[30]:


"""insertion updated"""
data_ins=data.drop(data.iloc[:,3:15],axis=1)
data_ins.rename(columns=data_ins.iloc[1], inplace = True)
update_ins = data_ins.drop([data_ins.index[0], data_ins.index[1]])
update_ins=update_ins.reset_index(drop=True)
update_ins = update_ins.dropna(subset=['Manufacturer'])
update_ins.drop(update_ins.loc[update_ins['Manufacturer']=='Manufacturer'].index, inplace=True)


# In[31]:


result_ins=pd.merge(update_ins, year_pd, left_index=True, right_index=True)
result_ins=pd.melt(result_ins, id_vars =['Manufacturer','Tier','Channel','year'],var_name='Months',value_name='Insertion')


# In[32]:


"""merging investment and insertion"""
data_merge=result_ins.merge(result_inv, left_on=['Manufacturer','Tier','Channel','Months','year'], right_on=['Manufacturer','Tier','Channel','Months','year'])


# In[33]:


data_merge.to_csv("gs://mmm-ford-de-bucket/input/Ford_real_data/real_data_processed.csv",index=False)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[20]:


data_merge=data_inv.merge(data_ins, left_on=['Manufacturer','Tier','Channel','Months','year'], right_on=['Manufacturer','Tier','Channel','Months','year'])


# In[307]:


# data_merge['Months']=data_merge['Months'].map(Month) 


# In[ ]:





# In[3]:


# data_merge.to_csv("gs://mmm-ford-de-bucket/input/Ford_real_data/real_data_final.csv")


# In[21]:





# In[23]:



 


# In[25]:





# In[27]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[402]:


import itertools
k = 16
istOfLists = [list(itertools.repeat(element, k)) for element in year]


# In[424]:


year_pd=pd.DataFrame(istOfLists)


# In[425]:


year_pd=pd.melt(year_pd,value_name='year')


# In[416]:


# year=pd.melt()
year=pd.DataFrame(year)


# In[426]:


year_pd.head(50)


# In[411]:


year.loc[year.index.repeat(year,16)]


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[400]:


from itertools import repeat


# In[367]:


l.extend(repeat(year, 16))


# In[369]:


year_pd=pd.DataFrame(l)


# In[370]:


l = [year for i in range(16)];


# In[371]:





# In[365]:


l=[]
for i in range(year):
    l.append(i)


# In[ ]:





# In[363]:


year_pd=pd.DataFrame(l)


# In[364]:


year


# In[ ]:





# In[ ]:





# In[ ]:


##########################


# In[421]:


"""insertion data"""
data_ins=data.drop(data.iloc[:,3:15],axis=1)
data_ins.rename(columns=data_ins.iloc[1], inplace = True)
data_ins = data_ins.drop(labels=[0,1], axis=0)
data_ins=pd.melt(data_ins,id_vars=['Manufacturer','Tier','Channel'],var_name='Months',value_name='Insertion')


# In[4]:





# In[578]:


pip install sparksql-magic


# In[425]:


data_inv.to_csv("gs://mmm-ford-de-bucket/input/Ford_real_data/data_inv.csv")
data_ins.to_csv("gs://mmm-ford-de-bucket/input/Ford_real_data/data_ins.csv")


# In[ ]:





# In[ ]:





# In[ ]:


########################################


# In[241]:


data_new=pd.read_csv('gs://mmm-ford-de-bucket/input/Ford_real_data/Real Data.csv')


# In[153]:


data_new=data_new.iloc[:,:15]
data_new.rename(columns=data_new.iloc[1], inplace = True)
# data_new=pd.melt(data_new,id_vars=['Manufacturer','Tier','Channel'],var_name='Months',value_name='Insertion')


# In[92]:


data_first=data_new.iloc[:,0:3]
index_names = data_first[ (data_first['Manufacturer'] == 'Manufacturer')].index
data_first.drop(index_names, inplace = True)
data_first.rename(columns=data_first.iloc[1], inplace = True)
data_first = data_first.drop(labels=[0], axis=0)
data_first=data_first.dropna()       


# In[288]:


"""year"""
data_second=data_new.iloc[:,3:]


# In[285]:


data_second.columns = data_second.iloc[0]
# data_second = data_second.rename(index={1: 'Year'})
# data_second = data_second.drop(labels=[0], axis=0)


# In[269]:


data_second.rename(columns=data_new.iloc[1], inplace = True)


# In[293]:


data_second=data_second.transpose()


# In[286]:


data_second.iloc[::,]


# In[296]:


data_second


# In[214]:


# data_yr=pd.DataFrame()
# for i in len(range(int(data_yr.iloc[:13,:])):
#              data_yr['year'] = data_second.iloc[0,:]


# In[ ]:





# In[46]:


get_ipython().run_line_magic('run..', '/utils/common_variables.ipynb')


# In[34]:


get_ipython().run_line_magic('run..', '/common_variables')


# In[40]:


df1


# In[ ]:




