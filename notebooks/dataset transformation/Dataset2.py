#!/usr/bin/env python
# coding: utf-8

# In[2]:


import pandas as pd
import numpy as np
from pathlib import Path
from google.cloud import storage
client = storage.Client()


# In[3]:


#Reading the file from google-stotage

inv_data= pd.read_excel('gs://mmm-ford-de-bucket/data/raw/Dataset2/SOI 2020 POR LINEA DIC REV02.xlsx')
ins_data= pd.read_excel('gs://mmm-ford-de-bucket/data/raw/Dataset2/SOI 2020 POR LINEA DIC REV03.xlsx')


# In[4]:


inv_data
# ins_data.head()


# In[38]:


# Channel Dict And Channel Extraction

channels = {'PRENSA': 'PRESS', 'RADIO': 'RADIO', 'REVISTA': 'MAGAZINE', 'TV ABIERTA': 'OPEN TV', 'TV PAGA': 'PAID TV'}

ch_df = inv_data.iloc[5:11,2:3]
ch_df = ch_df.reset_index(drop = True)
ch_df.rename(columns= ch_df.iloc[0], inplace = True)
ch_df.rename(columns = {'PRODUCTO' : 'Product', 'TIPO DE MEDIO' : 'Channel'}, inplace = True)
ch_df= ch_df.drop(labels=[0], axis=0)
ch_df['Channel'] = ch_df['Channel'].map(channels)
ch_df = ch_df.reset_index(drop = True)

ch_df


# In[4]:


# Product, Channel, Months and Investment Extraction

df = inv_data.iloc[5:,1:15]
df = df.reset_index(drop = True)
df.rename(columns=df.iloc[0], inplace = True)
df.rename(columns = {'PRODUCTO' : 'Product', 'TIPO DE MEDIO' : 'Channel'}, inplace = True)
df['Product']=df['Product'].fillna(method="ffill")

# df["year"]=df.apply(lambda x: x['year'] if pd.isna(x["PRODUCTO"]) else None,axis=1)
# df['year']=df['year'].fillna(method="ffill")
# df=df[~df["PRODUCTO"].isin([np.nan,"PRODUCTO"])]  
df= df.drop(labels=[0], axis=0)
df=pd.melt(df,id_vars=['Product','Channel'],var_name = 'Months', value_name = 'Investment')



Month={'ENERO':'Jan','FEBRERO':'Feb','MARZO':'Mar','ABRIL':'Apr','MAYO':'May','JUNIO':'June','JULIO':'Jul','AGOSTO':'Aug','SEPTIEMBRE':'Sep','OCTUBRE':'Oct','NOVIEMBRE':'Nov','DICIEMBRE':'Dec'}
df['Months'] = df['Months'].map(Month)
df.to_csv('gs://mmm-ford-de-bucket/data/raw/Dataset2/output.csv', index = False)
df.head(15)


# In[5]:


# Months Exctraction

Month={'ENERO':'Jan','FEBRERO':'Feb','MARZO':'Mar','ABRIL':'Apr','MAYO':'May','JUNIO':'June','JULIO':'Jul','AGOSTO':'Aug','SEPTIEMBRE':'Sep','OCTUBRE':'Oct','NOVIEMBRE':'Nov','DICIEMBRE':'Dec'}
months = inv_data.iloc[5:6,3:15]
# months = months.transpose()
# months = months.rename(columns=months.iloc[1], inplace = True) 
# months = months.drop(labels=[0,1], axis=0)
# months = months.rename(columns = {'5':'Months'})
months
# print(months.iloc[1])
# data_new=pd.melt(data_new,id_vars=['Manufacturer','Tier','Channel'],var_name='Months',value_name='Insertion')


months_new=pd.melt(months,value_name= 'Months')
months_new = months_new.drop(columns='variable', axis=1)
months_new['Months']=months_new['Months'].map(Month)
months_new


# In[6]:


# Year Extraction

df = inv_data.iloc[3:,1:15]
df = df.reset_index(drop = True)
df.rename(columns = df.iloc[2], inplace = True)
df.rename(columns = {'PRODUCTO' : 'Product', 'TIPO DE MEDIO' : 'Channel'}, inplace = True)
df['Product']=df['Product'].fillna(method="ffill")
df= df.drop(labels=[0], axis=0)
df=pd.melt(df,id_vars=['Product','Channel'],var_name = 'Months', value_name = 'Investment')
Month={'ENERO':'Jan','FEBRERO':'Feb','MARZO':'Mar','ABRIL':'Apr','MAYO':'May','JUNIO':'June','JULIO':'Jul','AGOSTO':'Aug','SEPTIEMBRE':'Sep','OCTUBRE':'Oct','NOVIEMBRE':'Nov','DICIEMBRE':'Dec'}
df['Months'] = df['Months'].map(Month)


df["year"]=df.apply(lambda x: x['Investment'] if pd.isna(x["Channel"]) else None,axis=1)

list(df[0:1]['Investment']) *16
df


# In[7]:


new_df = pd.DataFrame()
new_df


# In[8]:


# Extracting multiple dfs

file = pd.ExcelFile(r"gs://mmm-ford-de-bucket/data/raw/Dataset2/SOI 2020 POR LINEA DIC REV02.xlsx")

d = {name: pd.DataFrame() for name in file.sheet_names}
for name, df in d.items():
    d[name] = file.parse(name)
d


# In[9]:


# Extracting Category from the main_df

main_df = inv_data
main_df = main_df.iloc[2:3,1:15]
new_df = pd.melt(main_df,value_name= 'Category')
new_df = new_df.drop(columns='variable', axis=1)
new_df = new_df['Category'].str.split(': ', expand=True)
new_df = new_df.drop(columns=0, axis=1)
new_df.columns = ['Category']
new_df['Category'] = new_df['Category'].fillna(method="ffill")

new_df


# In[10]:


main_df = inv_data
main_df = main_df.iloc[:,1:16]
main_df


# In[ ]:




