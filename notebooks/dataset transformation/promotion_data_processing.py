#!/usr/bin/env python
# coding: utf-8

# In[255]:


import pandas as pd
import numpy as np


# In[256]:


import pandas as pd
from google.cloud import storage
client = storage.Client()
import numpy as np


# In[257]:


from google.cloud import storage

client = storage.Client()
bucket = client.bucket('mmm-ford-de-bucket') 

blobs = list(bucket.list_blobs(prefix='input/promotion'))
bucket = client.bucket('mmm-ford-de-bucket')
blobs = list(bucket.list_blobs(prefix='input/promotion'))
bb5 = blobs[1]
data = bb5.name[19:]


# In[258]:


df = pd.read_excel('gs://mmm-ford-de-bucket/input/promotion/2021-04-05_MAM_21MY SUVs.xlsx', sheet_name='Surrogates')


# In[210]:


df.dropna(axis = 0, how = 'all', inplace = True)
df.drop(columns=df.columns[0], 
        axis=1, 
        inplace=True)


# In[211]:


cols = ['Unnamed: 1', 'Unnamed: 2']
df.loc[:,cols] = df.loc[:,cols].ffill()
df = df.rename(columns={'Unnamed: 1': 'Year', 'Unnamed: 2': 'Model','Unnamed: 3':'UID','Unnamed: 4':'Make','Unnamed: 5':'Vehicle Line','Unnamed: 6':'Catalog','Unnamed: 7':'Model Year','Unnamed: 9':'MSRP','Unnamed: 11':'Cash Price','Unnamed: 13':'Fin Price','Unnamed: 15':'Program','Unnamed: 17':'% Fin Cost','Unnamed: 19':'%Fin Take Rate','Unnamed: 21':'Cash Incentive Cash','Unnamed: 23':'Cash Incentive Financing','Unnamed: 25':'Cash Incentive JATO Cash','Unnamed: 27':'Cash Incentive JATO Financing','Unnamed: 29':'Insurance Cash','Unnamed: 31':'Insurance Financing','Unnamed: 33':'Opening Fee Check','Unnamed: 35':'Opening Fee Financing','Unnamed: 37':'Free Service Cash','Unnamed: 39':'Free Service Financing','Unnamed: 41':'Other Incentive Cash','Unnamed: 43':'Other Incentive Financing','Unnamed: 45':'Average Mkg Cost PU Financing','Unnamed: 47':'Average Mkg Cost PU Cash','Unnamed: 49':'Average Mkg Cost PU Cash Av. @ Ford Take Rate'})


# In[212]:


df_new=['Year','Model','UID','Make','Vehicle Line','Catalog','Model Year','MSRP','Cash Price','Fin Price','Program','% Fin Cost','%Fin Take Rate','Cash Incentive Cash','Cash Incentive Financing','Cash Incentive JATO Cash','Cash Incentive JATO Financing','Insurance Cash','Insurance Financing','Opening Fee Check','Opening Fee Financing','Free Service Cash','Free Service Financing','Other Incentive Cash','Other Incentive Financing','Average Mkg Cost PU Financing','Average Mkg Cost PU Cash','Average Mkg Cost PU Cash Av. @ Ford Take Rate']


# In[213]:


df_=df[df.columns[df.columns.isin(['Year','Model','UID','Make','Vehicle Line','Catalog','Model Year','MSRP','Cash Price','Fin Price','Program','% Fin Cost','%Fin Take Rate','Cash Incentive Cash','Cash Incentive Financing','Cash Incentive JATO Cash','Cash Incentive JATO Financing','Insurance Cash','Insurance Financing','Opening Fee Check','Opening Fee Financing','Free Service Cash','Free Service Financing','Other Incentive Cash','Other Incentive Financing','Average Mkg Cost PU Financing','Average Mkg Cost PU Cash','Average Mkg Cost PU Cash Av. @ Ford Take Rate'])]]


# In[214]:


df=df_.reset_index(drop=True)


# In[215]:


df = df.drop([df.index[0] , df.index[1],df.index[2],df.index[3]])
df=df.reset_index(drop=True)


# In[216]:


df.drop(df.loc[df['UID']=='UID'].index, inplace=True)


# In[217]:


df=df.reset_index(drop=True)


# In[218]:


df.head()


# In[219]:


##########################################################


# In[220]:


"""processing the first ford sheet from the """


# In[297]:


data_2 = pd.read_excel('gs://mmm-ford-de-bucket/input/promotion/2021-04-05_MAM_21MY SUVs.xlsx', sheet_name='Ford')


# In[295]:


###################
data_yr=data_2.iloc[:,1]
data_yr=data_yr.iloc[5]
###################


# In[298]:


data_2=data_2.iloc[:,[1,2]]


# In[299]:


data_2


# In[ ]:





# In[289]:





# In[ ]:




