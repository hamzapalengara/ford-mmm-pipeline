#!/usr/bin/env python
# coding: utf-8

# In[42]:



import pandas as pd
import numpy as np


# In[43]:


from google.cloud import storage

client = storage.Client()
bucket = client.bucket('mmm-ford-de-bucket') 

blobs = list(bucket.list_blobs(prefix='input/jato_pricing'))
bucket = client.bucket('mmm-ford-de-bucket')
blobs = list(bucket.list_blobs(prefix='input/jato_pricing'))
bb5 = blobs[1]
data = bb5.name[19:]


# In[44]:


data


# In[45]:



df = pd.read_excel('gs://mmm-ford-de-bucket/input/jato_pricing/Prices.xlsx', sheet_name='JATO Worksheet - Sheet 1')


# In[46]:


import datetime
currentDateTime = datetime.datetime.now()
date = currentDateTime.date()
year1 = date.strftime("%Y")


# In[47]:


year = str(int(year1)-1)
year[-2:]


# In[48]:


l2 = year[-2:]
l1 = year1[-2:]


# In[49]:


column_needed = []
column = list(df.columns)
for i in df.columns:
    if i == 'Model':
        column_needed.append(i)
    if i == 'UID':
        column_needed.append(i)
    if i == 'Make':
        column_needed.append(i)
    if i[:2] == 'Pr':
        if i[-2:] == year[-2:]:
            column_needed.append(i)
        if i[-2:] == year1[-2:]:
            column_needed.append(i)
    


# In[50]:


column_needed


# In[51]:


dff =df[column_needed]


# In[52]:


dff


# In[53]:


# Renaming columns into a legible format
for i in dff.columns:
    if i not in ('Make','Model'):
        dff = dff.rename(columns={i : (i[-6:])})


# In[54]:


dff=dff.rename(columns={('Jan ' + l2): (year + '_01'),('Feb ' + l2): (year + '_02'),
('Mar ' + l2): (year + '_03'),
('Apr ' + l2): (year + '_04'),
('May ' + l2): (year + '_05'),
('Jun ' + l2): (year + '_06'),
('Jul ' + l2): (year + '_07'),
('Aug ' + l2): (year + '_08'),
('Sep ' + l2): (year + '_09'),
   ('Oct ' + l2): (year + '_10'),
   ('Nov ' + l2): (year + '_11'),
   ('Dec ' + l2): (year + '_12')})


# In[55]:


dff=dff.rename(columns={('Jan ' + l1): (year1 + '_01'),('Feb ' + l1): (year1 + '_02'),
('Mar ' + l1): (year1 + '_03'),
('Apr ' + l1): (year1 + '_04'),
('May ' + l1): (year1 + '_05'),
('Jun ' + l1): (year1 + '_06'),
('Jul ' + l1): (year1 + '_07'),
('Aug ' + l1): (year1 + '_08'),
('Sep ' + l1): (year1 + '_09'),
   ('Oct ' + l1): (year1 + '_10'),
   ('Nov ' + l1): (year1 + '_11'),
   ('Dec ' + l1): (year1 + '_12')})


# In[56]:


df1 = dff.groupby(['Model','Make'],as_index=False).sum()


# In[57]:


df_ans = df1.melt(['UID','Make','Model'],var_name='Period',value_name='Retail sales')


# In[58]:


df_ans = df_ans.reset_index()


# In[59]:


df_ans = df_ans.iloc[:,1:]


# In[60]:


df_ans1 = df_ans.rename(columns={'Model':'NamePlate'})


# In[ ]:




