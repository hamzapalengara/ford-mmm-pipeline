#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
from google.cloud import storage
client = storage.Client()
import numpy as np


# In[2]:


data=pd.read_excel('gs://mmm-ford-de-bucket/data/raw/Dataset3/SOI & SOV Digital Autos 2015-2021[1].xlsx',sheet_name='Inversión')


# In[3]:


data


# In[4]:


data.drop(['Unnamed: 13'],axis=1,inplace=True)


# In[5]:


data = data.fillna(0)


# In[6]:


data.head()


# In[7]:


data.drop(data.index[data['Unnamed: 0'] == 0], inplace=True)


# In[8]:


data.drop(data.index[data['Unnamed: 0'] == 'Total general'], inplace=True)


# In[9]:


data


# In[10]:


data=data.T


# In[11]:


data.head()


# In[12]:


data.columns = data.iloc[0]
data= data[1:]
data.head()


# In[13]:


data = data.reset_index(drop=True)


# In[14]:


data.head()


# In[15]:


z=list(data.columns)
c=z[2:13]


# In[16]:


df1 = pd.DataFrame(columns=['Year', 'Month', 'Manufacture', 'Inversión'])
df1
a=0
b=13
for i in range(int(len(data.columns)/13)):
    
    df=data.iloc[:,a:b]
    x=df.columns[0]
    d=pd.melt(df,id_vars=[x,'marca'],value_vars=c)
    d[x]=x
    d.rename(columns = {x:'Year', 'marca':'Month','Unnamed: 0':'Manufacture',
                              'value':'Inversión'}, inplace = True)
    df1=pd.concat([df1,d],axis=0)
    a=a+13
    b=b+13


# In[17]:


df1 = df1.reset_index(drop=True)


# In[18]:


df1['Month']=df1['Month'].astype('int64')


# In[19]:


df1


# In[20]:


df1.to_csv("gs://mmm-ford-de-bucket/output/Dataset3/Dataset3_Inversión.csv")


# In[ ]:





# In[21]:


data1=pd.read_excel('gs://mmm-ford-de-bucket/data/raw/Dataset3/SOI & SOV Digital Autos 2015-2021[1].xlsx',sheet_name='Impresiones')


# In[22]:


data1.head()


# In[23]:


data1.drop(['Unnamed: 13'],axis=1,inplace=True)


# In[24]:


data1.head()


# In[25]:


data1 = data1.fillna(0)


# In[26]:


data1.head()


# In[27]:


data1.drop(data1.index[data1['Unnamed: 0'] == 'Total general'], inplace=True)


# In[28]:


data1.drop(data1.index[data1['Unnamed: 0'] == 0], inplace=True)


# In[29]:


data1.head()


# In[30]:


data1=data1.T


# In[31]:


data1.head()


# In[32]:


data1.columns = data1.iloc[0]
data1= data1[1:]
data1.head()


# In[33]:


data1 = data1.reset_index(drop=True)


# In[34]:


data1.head()


# In[35]:


z1=list(data1.columns)
c1=z1[2:13]


# In[36]:


df2 = pd.DataFrame(columns=['Year', 'Month', 'Manufacture', 'Impresiones'])
a=0
b=13
for i in range(int(len(data1.columns)/13)):
    
    df3=data1.iloc[:,a:b]
    y=df3.columns[0]
    d1=pd.melt(df3,id_vars=[y,'marca'],value_vars=c1)
    d1[y]=y
    d1.rename(columns = {y:'Year', 'marca':'Month','Unnamed: 0':'Manufacture',
                              'value':'Impresiones'}, inplace = True)
    df2=pd.concat([df2,d1],axis=0)
    a=a+13
    b=b+13


# In[37]:


df2 = df2.reset_index(drop=True)


# In[38]:


df2['Month']=df2['Month'].astype('int64')


# In[39]:


df2.head()


# In[40]:


df2.to_csv("gs://mmm-ford-de-bucket/output/Dataset3/Dataset3_Impresiones.csv")

