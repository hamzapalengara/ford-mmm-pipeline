#!/usr/bin/env python
# coding: utf-8

# # 2016

# In[1]:


import pandas as pd
from google.cloud import storage
client = storage.Client()
import numpy as np


# In[2]:


path='gs://mmm-ford-de-bucket/data/raw/Sales_Data(AMIA)/Retail2016.xls'


# In[3]:


num = ""
for c in path:
    if c.isdigit():
        num = num + c
print("Extracted numbers from the list : " + num) 

if len(num)==6:
    yr=int(num[2::])
else:
    yr=int(num)


# In[4]:


yr


# In[5]:


data=pd.read_excel(path,sheet_name='AMIA')


# In[6]:


data.head()


# In[7]:


def getIndexes(dfObj, value):
    ''' Get index positions of value in dataframe.'''
    listOfPos = list()
    # Get bool dataframe with True at positions where the given value exists
    result = dfObj.isin([value])
    # Get list of columns that contains the value
    seriesObj = result.any()
    columnNames = list(seriesObj[seriesObj == True].index)
    # Iterate over list of columns and fetch the rows indexes where value exists
    for col in columnNames:
        rows = list(result[col][result[col] == True].index)
        for row in rows:
            listOfPos.append((row, col))
    # Return a list of tuples indicating the positions of value in the dataframe
    return listOfPos


# In[8]:


listOfPositions = getIndexes(data,'Figo')
listOfPositions


# In[9]:


m=listOfPositions[0][1]
m


# In[10]:


x=listOfPositions[0][0]
x


# In[11]:


data=data.iloc[x:,0:14]


# In[12]:


data.head()


# In[13]:


data.drop([data.columns[0]],axis=1,inplace=True)


# In[14]:


data.reset_index(inplace=True)


# In[15]:


data.drop([data.columns[0]],axis=1,inplace=True)


# In[16]:


data.head()


# In[17]:


data.rename(columns = {m:'Car Name', 'Unnamed: 2':1,'Unnamed: 3':2,'Unnamed: 4':3,
                      'Unnamed: 5':4,'Unnamed: 6':5,'Unnamed: 7':6,'Unnamed: 8':7,
                      'Unnamed: 9':8,'Unnamed: 10':9,'Unnamed: 11':10,'Unnamed: 12':11,'Unnamed: 13':12}, inplace = True)


# In[18]:


data.head()


# In[19]:


#Mapping Data


# In[20]:


df=pd.read_excel('gs://mmm-ford-de-bucket/data/mapping/Mapping v1.1.xlsx',sheet_name='Segment')


# In[21]:


df


# In[22]:


df.columns = df.iloc[0]
df= df[1:]
df.head()


# In[23]:


df.drop(['Car Name-new'],axis=1,inplace=True)


# In[24]:


data1=df.merge(data, how='inner', on='Car Name')


# In[25]:


data1.head()


# In[26]:


x = data1["Manufacturer"] +'@'+ data1["Car Name"]+'@'+data1["Segment"]+'@'+data1["Sub_Segment"]


# In[27]:


data1.insert(loc = 0,column = 'Name',value = x)


# In[28]:


data1.head()


# In[29]:


data1.drop(['Manufacturer','Car Name','Segment','Sub_Segment'],axis=1,inplace=True)


# In[30]:


data1.head()


# In[31]:


data1=data1.T


# In[32]:


data1.reset_index(inplace=True)


# In[33]:


data1.head()


# In[34]:


data1.columns = data1.iloc[0]
data1= data1[1:]
data1.head()


# In[35]:


data1.head()


# In[36]:


d=pd.melt(data1,id_vars=['Name'])


# In[37]:


d.head()


# In[38]:


new = d[0].str.split("@", n = 3, expand = True)
  
d["Manufacturer"]= new[0]
d["Car Name"]= new[1]
d['Segment']=new[2]
d['Sub_Segment']=new[3]
  
# Dropping old Name columns
d.drop(columns =[0], inplace = True)
  
# df display
d


# In[39]:


d.rename(columns = {'Name':'Month', 'value':'Sales'}, inplace = True)


# In[40]:


d.head()


# In[41]:


d.insert(0,'Year',yr)


# In[42]:


d.drop_duplicates(inplace=True)


# In[43]:


d.drop(d.index[d['Car Name'] =='Focus Total'], inplace=True)


# In[44]:


d.drop(d.index[d['Car Name'] =='F-Series SD'], inplace=True)


# In[45]:


#removing Premium and (blank) segment..


# In[46]:


y=d[(d['Segment']!='Premium')&(d['Segment']!='(blank)')]
y=y.fillna(0)
y.to_csv("gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2016_Sales.csv")


# In[47]:


y


# In[48]:


x=d[(d['Segment']!='Premium')&(d['Segment']!='(blank)')]


# In[49]:


x.Segment.value_counts()


# In[50]:


x=x.fillna(0)


# In[51]:


x.max()


# In[52]:


x.columns


# In[53]:


x['Sales'].value_counts()


# In[54]:


x.dtypes


# In[55]:


x['Sales']=x['Sales'].astype('int64')


# In[56]:


x['Sales'].min()


# In[57]:


r=[i for i in range(2,x['Sales'].max()+1)]
s=[i for i in range(x['Sales'].min()-1,0)]


# In[58]:


x['Sales']=x['Sales'].replace(r,1)


# In[59]:


x['Sales']=x['Sales'].replace(s,0)


# In[60]:


x.Sales.value_counts()


# In[61]:


x.Segment.value_counts()


# In[62]:


#for Ford


# In[63]:


sku_Ford=x[x['Manufacturer']=='Ford']


# In[64]:


sku_Ford.head()


# In[65]:


sku_Ford['Sales'].sum()


# In[66]:


z=sku_Ford.groupby(sku_Ford["Month"]).Sales.agg(["sum"])
                                           


# In[67]:


z.reset_index(inplace=True)


# In[68]:


z.insert(0,'Year',yr)


# In[69]:


z.rename(columns={'sum':'Ford_sku_count'},inplace=True)


# In[70]:


z


# In[71]:


#for comp


# In[72]:


sku_Comp=x[x['Manufacturer']!='Ford']


# In[73]:


sku_Comp.head()


# In[74]:


sku_Comp['Sales'].sum()


# In[75]:


w=sku_Comp.groupby(sku_Comp["Month"]).Sales.agg(["sum"])


# In[76]:


w.reset_index(inplace=True)


# In[77]:


w.insert(0,'Year',yr)


# In[78]:


w.rename(columns={'sum':'Comp_sku_count'},inplace=True)


# In[79]:


w


# In[80]:


sku1=pd.concat([z,w],axis=1)
sku1 = sku1.T.drop_duplicates().T
sku1


# In[81]:


sku1.to_csv("gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2016_sku_count.csv")


# In[ ]:





# # 2017

# In[82]:


import pandas as pd
from google.cloud import storage
client = storage.Client()
import numpy as np


# In[83]:


path='gs://mmm-ford-de-bucket/data/raw/Sales_Data(AMIA)/Retail2017.xls'


# In[84]:


num = ""
for c in path:
    if c.isdigit():
        num = num + c
print("Extracted numbers from the list : " + num) 

if len(num)==6:
    yr=int(num[2::])
else:
    yr=int(num)


# In[85]:


yr


# In[86]:


data=pd.read_excel(path,sheet_name='AMIA')


# In[87]:


data.head()


# In[88]:


def getIndexes(dfObj, value):
    ''' Get index positions of value in dataframe.'''
    listOfPos = list()
    # Get bool dataframe with True at positions where the given value exists
    result = dfObj.isin([value])
    # Get list of columns that contains the value
    seriesObj = result.any()
    columnNames = list(seriesObj[seriesObj == True].index)
    # Iterate over list of columns and fetch the rows indexes where value exists
    for col in columnNames:
        rows = list(result[col][result[col] == True].index)
        for row in rows:
            listOfPos.append((row, col))
    # Return a list of tuples indicating the positions of value in the dataframe
    return listOfPos


# In[89]:


listOfPositions = getIndexes(data,'Figo')
listOfPositions


# In[90]:


m=listOfPositions[0][1]
m


# In[91]:


x=listOfPositions[0][0]
x


# In[92]:


data=data.iloc[x:,0:14]


# In[93]:


data.head()


# In[94]:


data.drop([data.columns[0]],axis=1,inplace=True)


# In[95]:


data.reset_index(inplace=True)


# In[96]:


data.drop([data.columns[0]],axis=1,inplace=True)


# In[97]:


data.head()


# In[98]:


data.rename(columns = {m:'Car Name', 'Unnamed: 2':1,'Unnamed: 3':2,'Unnamed: 4':3,
                      'Unnamed: 5':4,'Unnamed: 6':5,'Unnamed: 7':6,'Unnamed: 8':7,
                      'Unnamed: 9':8,'Unnamed: 10':9,'Unnamed: 11':10,'Unnamed: 12':11,'Unnamed: 13':12}, inplace = True)


# In[99]:


data.head()


# In[100]:


#Mapping Data


# In[101]:


df=pd.read_excel('gs://mmm-ford-de-bucket/data/mapping/Mapping v1.1.xlsx',sheet_name='Segment')


# In[102]:


df


# In[103]:


df.columns = df.iloc[0]
df= df[1:]
df.head()


# In[104]:


df.drop(['Car Name-new'],axis=1,inplace=True)


# In[105]:


data1=df.merge(data, how='inner', on='Car Name')


# In[106]:


data1.head()


# In[107]:


x = data1["Manufacturer"] +'@'+ data1["Car Name"]+'@'+data1["Segment"]+'@'+data1["Sub_Segment"]


# In[108]:


data1.insert(loc = 0,column = 'Name',value = x)


# In[109]:


data1.head()


# In[110]:


data1.drop(['Manufacturer','Car Name','Segment','Sub_Segment'],axis=1,inplace=True)


# In[111]:


data1.head()


# In[112]:


data1=data1.T


# In[113]:


data1.reset_index(inplace=True)


# In[114]:


data1.head()


# In[115]:


data1.columns = data1.iloc[0]
data1= data1[1:]
data1.head()


# In[116]:


data1.head()


# In[117]:


d=pd.melt(data1,id_vars=['Name'])


# In[118]:


d.head()


# In[119]:


new = d[0].str.split("@", n = 3, expand = True)
  
d["Manufacturer"]= new[0]
d["Car Name"]= new[1]
d['Segment']=new[2]
d['Sub_Segment']=new[3]
  
# Dropping old Name columns
d.drop(columns =[0], inplace = True)
  
# df display
d


# In[120]:


d.rename(columns = {'Name':'Month', 'value':'Sales'}, inplace = True)


# In[121]:


d.head()


# In[122]:


d.insert(0,'Year',yr)


# In[123]:


d.drop_duplicates(inplace=True)


# In[124]:


d.drop(d.index[d['Car Name'] =='Focus Total'], inplace=True)


# In[125]:


d.drop(d.index[d['Car Name'] =='F-Series SD'], inplace=True)


# In[126]:


#removing Premium and (blank) segment..


# In[127]:


y=d[(d['Segment']!='Premium')&(d['Segment']!='(blank)')]
y=y.fillna(0)
y.to_csv("gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2017_Sales.csv")


# In[128]:


x=d[(d['Segment']!='Premium')&(d['Segment']!='(blank)')]


# In[129]:


x.Segment.value_counts()


# In[130]:


x=x.fillna(0)


# In[131]:


x.max()


# In[132]:


x.columns


# In[133]:


x['Sales'].value_counts()


# In[134]:


x.dtypes


# In[135]:


x['Sales']=x['Sales'].astype('int64')


# In[136]:


x['Sales'].min()


# In[137]:


r=[i for i in range(2,x['Sales'].max()+1)]
s=[i for i in range(x['Sales'].min()-1,0)]


# In[138]:


x['Sales']=x['Sales'].replace(r,1)


# In[139]:


x['Sales']=x['Sales'].replace(s,0)


# In[140]:


x.Sales.value_counts()


# In[141]:


x.Segment.value_counts()


# In[142]:


#for Ford


# In[143]:


sku_Ford=x[x['Manufacturer']=='Ford']


# In[144]:


sku_Ford.head()


# In[145]:


sku_Ford['Sales'].sum()


# In[146]:


z=sku_Ford.groupby(sku_Ford["Month"]).Sales.agg(["sum"])
                                           


# In[147]:


z.reset_index(inplace=True)


# In[148]:


z.insert(0,'Year',yr)


# In[149]:


z.rename(columns={'sum':'Ford_sku_count'},inplace=True)


# In[150]:


z


# In[151]:


#for comp


# In[152]:


sku_Comp=x[x['Manufacturer']!='Ford']


# In[153]:


sku_Comp.head()


# In[154]:


sku_Comp['Sales'].sum()


# In[155]:


w=sku_Comp.groupby(sku_Comp["Month"]).Sales.agg(["sum"])


# In[156]:


w.reset_index(inplace=True)


# In[157]:


w.insert(0,'Year',yr)


# In[158]:


w.rename(columns={'sum':'Comp_sku_count'},inplace=True)


# In[159]:


w


# In[160]:


sku1=pd.concat([z,w],axis=1)
sku1 = sku1.T.drop_duplicates().T
sku1


# In[161]:


sku1.to_csv("gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2017_sku_count.csv")


# In[ ]:





# # 2018

# In[162]:


import pandas as pd
from google.cloud import storage
client = storage.Client()
import numpy as np


# In[163]:


path='gs://mmm-ford-de-bucket/data/raw/Sales_Data(AMIA)/Retail2018FY.xls'


# In[164]:


num = ""
for c in path:
    if c.isdigit():
        num = num + c
print("Extracted numbers from the list : " + num) 

if len(num)==6:
    yr=int(num[2::])
else:
    yr=int(num)


# In[165]:


yr


# In[166]:


data=pd.read_excel(path,sheet_name='AMIA')


# In[167]:


data.head()


# In[168]:


def getIndexes(dfObj, value):
    ''' Get index positions of value in dataframe.'''
    listOfPos = list()
    # Get bool dataframe with True at positions where the given value exists
    result = dfObj.isin([value])
    # Get list of columns that contains the value
    seriesObj = result.any()
    columnNames = list(seriesObj[seriesObj == True].index)
    # Iterate over list of columns and fetch the rows indexes where value exists
    for col in columnNames:
        rows = list(result[col][result[col] == True].index)
        for row in rows:
            listOfPos.append((row, col))
    # Return a list of tuples indicating the positions of value in the dataframe
    return listOfPos


# In[169]:


listOfPositions = getIndexes(data,'Figo')
listOfPositions


# In[170]:


m=listOfPositions[0][1]
m


# In[171]:


x=listOfPositions[0][0]
x


# In[172]:


data=data.iloc[x:,0:14]


# In[173]:


data.head()


# In[174]:


data.drop([data.columns[0]],axis=1,inplace=True)


# In[175]:


data.reset_index(inplace=True)


# In[176]:


data.drop([data.columns[0]],axis=1,inplace=True)


# In[177]:


data.head()


# In[178]:


data.rename(columns = {m:'Car Name', 'Unnamed: 2':1,'Unnamed: 3':2,'Unnamed: 4':3,
                      'Unnamed: 5':4,'Unnamed: 6':5,'Unnamed: 7':6,'Unnamed: 8':7,
                      'Unnamed: 9':8,'Unnamed: 10':9,'Unnamed: 11':10,'Unnamed: 12':11,'Unnamed: 13':12}, inplace = True)


# In[179]:


data.head()


# In[180]:


#Mapping Data


# In[181]:


df=pd.read_excel('gs://mmm-ford-de-bucket/data/mapping/Mapping v1.1.xlsx',sheet_name='Segment')


# In[182]:


df


# In[183]:


df.columns = df.iloc[0]
df= df[1:]
df.head()


# In[184]:


df.drop(['Car Name-new'],axis=1,inplace=True)


# In[185]:


data1=df.merge(data, how='inner', on='Car Name')


# In[186]:


data1.head()


# In[187]:


x = data1["Manufacturer"] +'@'+ data1["Car Name"]+'@'+data1["Segment"]+'@'+data1["Sub_Segment"]


# In[188]:


data1.insert(loc = 0,column = 'Name',value = x)


# In[189]:


data1.head()


# In[190]:


data1.drop(['Manufacturer','Car Name','Segment','Sub_Segment'],axis=1,inplace=True)


# In[191]:


data1.head()


# In[192]:


data1=data1.T


# In[193]:


data1.reset_index(inplace=True)


# In[194]:


data1.head()


# In[195]:


data1.columns = data1.iloc[0]
data1= data1[1:]
data1.head()


# In[196]:


data1.head()


# In[197]:


d=pd.melt(data1,id_vars=['Name'])


# In[198]:


d.head()


# In[199]:


new = d[0].str.split("@", n = 3, expand = True)
  
d["Manufacturer"]= new[0]
d["Car Name"]= new[1]
d['Segment']=new[2]
d['Sub_Segment']=new[3]
  
# Dropping old Name columns
d.drop(columns =[0], inplace = True)
  
# df display
d


# In[200]:


d.rename(columns = {'Name':'Month', 'value':'Sales'}, inplace = True)


# In[201]:


d.head()


# In[202]:


d.insert(0,'Year',yr)


# In[203]:


d.drop_duplicates(inplace=True)


# In[204]:


d.drop(d.index[d['Car Name'] =='Focus Total'], inplace=True)


# In[205]:


d.drop(d.index[d['Car Name'] =='F-Series SD'], inplace=True)


# In[206]:


#removing Premium and (blank) segment..


# In[207]:


y=d[(d['Segment']!='Premium')&(d['Segment']!='(blank)')]
y=y.fillna(0)
y.to_csv("gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2018_Sales.csv")


# In[208]:


x=d[(d['Segment']!='Premium')&(d['Segment']!='(blank)')]


# In[209]:


x.Segment.value_counts()


# In[210]:


x=x.fillna(0)


# In[211]:


x.max()


# In[212]:


x.columns


# In[213]:


x['Sales'].value_counts()


# In[214]:


x.dtypes


# In[215]:


x['Sales']=x['Sales'].astype('int64')


# In[216]:


x['Sales'].min()


# In[217]:


r=[i for i in range(2,x['Sales'].max()+1)]
s=[i for i in range(x['Sales'].min()-1,0)]


# In[218]:


x['Sales']=x['Sales'].replace(r,1)


# In[219]:


x['Sales']=x['Sales'].replace(s,0)


# In[220]:


x.Sales.value_counts()


# In[221]:


x.Segment.value_counts()


# In[222]:


#for Ford


# In[223]:


sku_Ford=x[x['Manufacturer']=='Ford']


# In[224]:


sku_Ford.head()


# In[225]:


sku_Ford['Sales'].sum()


# In[226]:


z=sku_Ford.groupby(sku_Ford["Month"]).Sales.agg(["sum"])
                                           


# In[227]:


z.reset_index(inplace=True)


# In[228]:


z.insert(0,'Year',yr)


# In[229]:


z.rename(columns={'sum':'Ford_sku_count'},inplace=True)


# In[230]:


z


# In[231]:


#for comp


# In[232]:


sku_Comp=x[x['Manufacturer']!='Ford']


# In[233]:


sku_Comp.head()


# In[234]:


sku_Comp['Sales'].sum()


# In[235]:


w=sku_Comp.groupby(sku_Comp["Month"]).Sales.agg(["sum"])


# In[236]:


w.reset_index(inplace=True)


# In[237]:


w.insert(0,'Year',yr)


# In[238]:


w.rename(columns={'sum':'Comp_sku_count'},inplace=True)


# In[239]:


w


# In[240]:


sku1=pd.concat([z,w],axis=1)
sku1 = sku1.T.drop_duplicates().T
sku1


# In[241]:


sku1.to_csv("gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2018_sku_count.csv")


# In[ ]:





# # 2019

# In[242]:


import pandas as pd
from google.cloud import storage
client = storage.Client()
import numpy as np


# In[243]:


path='gs://mmm-ford-de-bucket/data/raw/Sales_Data(AMIA)/Retail2019.xlsx'


# In[244]:


num = ""
for c in path:
    if c.isdigit():
        num = num + c
print("Extracted numbers from the list : " + num) 

if len(num)==6:
    yr=int(num[2::])
else:
    yr=int(num)


# In[245]:


yr


# In[246]:


data=pd.read_excel(path,sheet_name='AMIA')


# In[247]:


data.head()


# In[248]:


def getIndexes(dfObj, value):
    ''' Get index positions of value in dataframe.'''
    listOfPos = list()
    # Get bool dataframe with True at positions where the given value exists
    result = dfObj.isin([value])
    # Get list of columns that contains the value
    seriesObj = result.any()
    columnNames = list(seriesObj[seriesObj == True].index)
    # Iterate over list of columns and fetch the rows indexes where value exists
    for col in columnNames:
        rows = list(result[col][result[col] == True].index)
        for row in rows:
            listOfPos.append((row, col))
    # Return a list of tuples indicating the positions of value in the dataframe
    return listOfPos


# In[249]:


listOfPositions = getIndexes(data,'Figo')
listOfPositions


# In[250]:


m=listOfPositions[0][1]
m


# In[251]:


x=listOfPositions[0][0]
x


# In[252]:


data=data.iloc[x:,0:14]


# In[253]:


data.head()


# In[254]:


data.drop([data.columns[0]],axis=1,inplace=True)


# In[255]:


data.reset_index(inplace=True)


# In[256]:


data.drop([data.columns[0]],axis=1,inplace=True)


# In[257]:


data.head()


# In[258]:


data.rename(columns = {m:'Car Name', 'Unnamed: 2':1,'Unnamed: 3':2,'Unnamed: 4':3,
                      'Unnamed: 5':4,'Unnamed: 6':5,'Unnamed: 7':6,'Unnamed: 8':7,
                      'Unnamed: 9':8,'Unnamed: 10':9,'Unnamed: 11':10,'Unnamed: 12':11,'Unnamed: 13':12}, inplace = True)


# In[259]:


data.head()


# In[260]:


#Mapping Data


# In[261]:


df=pd.read_excel('gs://mmm-ford-de-bucket/data/mapping/Mapping v1.1.xlsx',sheet_name='Segment')


# In[262]:


df


# In[263]:


df.columns = df.iloc[0]
df= df[1:]
df.head()


# In[264]:


df.drop(['Car Name-new'],axis=1,inplace=True)


# In[265]:


data1=df.merge(data, how='inner', on='Car Name')


# In[266]:


data1.head()


# In[267]:


x = data1["Manufacturer"] +'@'+ data1["Car Name"]+'@'+data1["Segment"]+'@'+data1["Sub_Segment"]


# In[268]:


data1.insert(loc = 0,column = 'Name',value = x)


# In[269]:


data1.head()


# In[270]:


data1.drop(['Manufacturer','Car Name','Segment','Sub_Segment'],axis=1,inplace=True)


# In[271]:


data1.head()


# In[272]:


data1=data1.T


# In[273]:


data1.reset_index(inplace=True)


# In[274]:


data1.head()


# In[275]:


data1.columns = data1.iloc[0]
data1= data1[1:]
data1.head()


# In[276]:


data1.head()


# In[277]:


d=pd.melt(data1,id_vars=['Name'])


# In[278]:


d.head()


# In[279]:


new = d[0].str.split("@", n = 3, expand = True)
  
d["Manufacturer"]= new[0]
d["Car Name"]= new[1]
d['Segment']=new[2]
d['Sub_Segment']=new[3]
  
# Dropping old Name columns
d.drop(columns =[0], inplace = True)
  
# df display
d


# In[280]:


d.rename(columns = {'Name':'Month', 'value':'Sales'}, inplace = True)


# In[281]:


d.head()


# In[282]:


d.insert(0,'Year',yr)


# In[283]:


d.drop_duplicates(inplace=True)


# In[284]:


d.drop(d.index[d['Car Name'] =='Focus Total'], inplace=True)


# In[285]:


d.drop(d.index[d['Car Name'] =='F-Series SD'], inplace=True)


# In[286]:


#removing Premium and (blank) segment..


# In[287]:


y=d[(d['Segment']!='Premium')&(d['Segment']!='(blank)')]
y=y.fillna(0)
y.to_csv("gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2019_Sales.csv")


# In[288]:


x=d[(d['Segment']!='Premium')&(d['Segment']!='(blank)')]


# In[289]:


x.Segment.value_counts()


# In[290]:


x=x.fillna(0)


# In[291]:


x.max()


# In[292]:


x.columns


# In[293]:


x['Sales'].value_counts()


# In[294]:


x.dtypes


# In[295]:


x['Sales']=x['Sales'].astype('int64')


# In[296]:


x['Sales'].min()


# In[297]:


r=[i for i in range(2,x['Sales'].max()+1)]
s=[i for i in range(x['Sales'].min()-1,0)]


# In[298]:


x['Sales']=x['Sales'].replace(r,1)


# In[299]:


x['Sales']=x['Sales'].replace(s,0)


# In[300]:


x.Sales.value_counts()


# In[301]:


x.Segment.value_counts()


# In[302]:


#for Ford


# In[303]:


sku_Ford=x[x['Manufacturer']=='Ford']


# In[304]:


sku_Ford.head()


# In[305]:


sku_Ford['Sales'].sum()


# In[306]:


z=sku_Ford.groupby(sku_Ford["Month"]).Sales.agg(["sum"])
                                           


# In[307]:


z.reset_index(inplace=True)


# In[308]:


z.insert(0,'Year',yr)


# In[309]:


z.rename(columns={'sum':'Ford_sku_count'},inplace=True)


# In[310]:


z


# In[311]:


#for comp


# In[312]:


sku_Comp=x[x['Manufacturer']!='Ford']


# In[313]:


sku_Comp.head()


# In[314]:


sku_Comp['Sales'].sum()


# In[315]:


w=sku_Comp.groupby(sku_Comp["Month"]).Sales.agg(["sum"])


# In[316]:


w.reset_index(inplace=True)


# In[317]:


w.insert(0,'Year',yr)


# In[318]:


w.rename(columns={'sum':'Comp_sku_count'},inplace=True)


# In[319]:


w


# In[320]:


sku1=pd.concat([z,w],axis=1)
sku1 = sku1.T.drop_duplicates().T
sku1


# In[321]:


sku1.to_csv("gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2019_sku_count.csv")


# In[ ]:





# # 2020

# In[322]:


import pandas as pd
from google.cloud import storage
client = storage.Client()
import numpy as np


# In[323]:


path='gs://mmm-ford-de-bucket/data/raw/Sales_Data(AMIA)/Retail2020 Diciembre.xlsx'


# In[324]:


num = ""
for c in path:
    if c.isdigit():
        num = num + c
print("Extracted numbers from the list : " + num) 

if len(num)==6:
    yr=int(num[2::])
else:
    yr=int(num)


# In[325]:


yr


# In[326]:


data=pd.read_excel(path,sheet_name='AMIA')


# In[327]:


data.head()


# In[328]:


def getIndexes(dfObj, value):
    ''' Get index positions of value in dataframe.'''
    listOfPos = list()
    # Get bool dataframe with True at positions where the given value exists
    result = dfObj.isin([value])
    # Get list of columns that contains the value
    seriesObj = result.any()
    columnNames = list(seriesObj[seriesObj == True].index)
    # Iterate over list of columns and fetch the rows indexes where value exists
    for col in columnNames:
        rows = list(result[col][result[col] == True].index)
        for row in rows:
            listOfPos.append((row, col))
    # Return a list of tuples indicating the positions of value in the dataframe
    return listOfPos


# In[329]:


listOfPositions = getIndexes(data,'Figo')
listOfPositions


# In[330]:


m=listOfPositions[0][1]
m


# In[331]:


x=listOfPositions[0][0]
x


# In[332]:


data=data.iloc[x:,0:14]


# In[333]:


data.head()


# In[334]:


data.drop([data.columns[0]],axis=1,inplace=True)


# In[335]:


data.reset_index(inplace=True)


# In[336]:


data.drop([data.columns[0]],axis=1,inplace=True)


# In[337]:


data.head()


# In[338]:


data.rename(columns = {m:'Car Name', 'Unnamed: 2':1,'Unnamed: 3':2,'Unnamed: 4':3,
                      'Unnamed: 5':4,'Unnamed: 6':5,'Unnamed: 7':6,'Unnamed: 8':7,
                      'Unnamed: 9':8,'Unnamed: 10':9,'Unnamed: 11':10,'Unnamed: 12':11,'Unnamed: 13':12}, inplace = True)


# In[339]:


data.head()


# In[340]:


#Mapping Data


# In[341]:


df=pd.read_excel('gs://mmm-ford-de-bucket/data/mapping/Mapping v1.1.xlsx',sheet_name='Segment')


# In[342]:


df


# In[343]:


df.columns = df.iloc[0]
df= df[1:]
df.head()


# In[344]:


df.drop(['Car Name-new'],axis=1,inplace=True)


# In[345]:


data1=df.merge(data, how='inner', on='Car Name')


# In[346]:


data1.head()


# In[347]:


x = data1["Manufacturer"] +'@'+ data1["Car Name"]+'@'+data1["Segment"]+'@'+data1["Sub_Segment"]


# In[348]:


data1.insert(loc = 0,column = 'Name',value = x)


# In[349]:


data1.head()


# In[350]:


data1.drop(['Manufacturer','Car Name','Segment','Sub_Segment'],axis=1,inplace=True)


# In[351]:


data1.head()


# In[352]:


data1=data1.T


# In[353]:


data1.reset_index(inplace=True)


# In[354]:


data1.head()


# In[355]:


data1.columns = data1.iloc[0]
data1= data1[1:]
data1.head()


# In[356]:


data1.head()


# In[357]:


d=pd.melt(data1,id_vars=['Name'])


# In[358]:


d.head()


# In[359]:


new = d[0].str.split("@", n = 3, expand = True)
  
d["Manufacturer"]= new[0]
d["Car Name"]= new[1]
d['Segment']=new[2]
d['Sub_Segment']=new[3]
  
# Dropping old Name columns
d.drop(columns =[0], inplace = True)
  
# df display
d


# In[360]:


d.rename(columns = {'Name':'Month', 'value':'Sales'}, inplace = True)


# In[361]:


d.head()


# In[362]:


d.insert(0,'Year',yr)


# In[363]:


d.drop_duplicates(inplace=True)


# In[364]:


d.drop(d.index[d['Car Name'] =='Focus Total'], inplace=True)


# In[365]:


d.drop(d.index[d['Car Name'] =='F-Series SD'], inplace=True)


# In[366]:


#removing Premium and (blank) segment..


# In[367]:


y=d[(d['Segment']!='Premium')&(d['Segment']!='(blank)')]
y=y.fillna(0)
y.to_csv("gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2020_Sales.csv")


# In[368]:


x=d[(d['Segment']!='Premium')&(d['Segment']!='(blank)')]


# In[369]:


x.Segment.value_counts()


# In[370]:


x=x.fillna(0)


# In[371]:


x.max()


# In[372]:


x.columns


# In[373]:


x['Sales'].value_counts()


# In[374]:


x.dtypes


# In[375]:


x['Sales']=x['Sales'].astype('int64')


# In[376]:


x['Sales'].min()


# In[377]:


r=[i for i in range(2,x['Sales'].max()+1)]
s=[i for i in range(x['Sales'].min()-1,0)]


# In[378]:


x['Sales']=x['Sales'].replace(r,1)


# In[379]:


x['Sales']=x['Sales'].replace(s,0)


# In[380]:


x.Sales.value_counts()


# In[381]:


x.Segment.value_counts()


# In[382]:


#for Ford


# In[383]:


sku_Ford=x[x['Manufacturer']=='Ford']


# In[384]:


sku_Ford.head()


# In[385]:


sku_Ford['Sales'].sum()


# In[386]:


z=sku_Ford.groupby(sku_Ford["Month"]).Sales.agg(["sum"])
                                           


# In[387]:


z.reset_index(inplace=True)


# In[388]:


z.insert(0,'Year',yr)


# In[389]:


z.rename(columns={'sum':'Ford_sku_count'},inplace=True)


# In[390]:


z


# In[391]:


#for comp


# In[392]:


sku_Comp=x[x['Manufacturer']!='Ford']


# In[393]:


sku_Comp.head()


# In[394]:


sku_Comp['Sales'].sum()


# In[395]:


w=sku_Comp.groupby(sku_Comp["Month"]).Sales.agg(["sum"])


# In[396]:


w.reset_index(inplace=True)


# In[397]:


w.insert(0,'Year',yr)


# In[398]:


w.rename(columns={'sum':'Comp_sku_count'},inplace=True)


# In[399]:


w


# In[400]:


sku1=pd.concat([z,w],axis=1)
sku1 = sku1.T.drop_duplicates().T
sku1


# In[401]:


sku1.to_csv("gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2020_sku_count.csv")


# In[ ]:





# # 2021

# In[402]:


import pandas as pd
from google.cloud import storage
client = storage.Client()
import numpy as np


# In[403]:


path='gs://mmm-ford-de-bucket/data/raw/Sales_Data(AMIA)/12 Retail2021 dic.xlsx'


# In[404]:


num = ""
for c in path:
    if c.isdigit():
        num = num + c
print("Extracted numbers from the list : " + num) 

if len(num)==6:
    yr=int(num[2::])
else:
    yr=int(num)


# In[405]:


yr


# In[406]:


data=pd.read_excel(path,sheet_name='AMIA')


# In[407]:


data.head()


# In[408]:


def getIndexes(dfObj, value):
    ''' Get index positions of value in dataframe.'''
    listOfPos = list()
    # Get bool dataframe with True at positions where the given value exists
    result = dfObj.isin([value])
    # Get list of columns that contains the value
    seriesObj = result.any()
    columnNames = list(seriesObj[seriesObj == True].index)
    # Iterate over list of columns and fetch the rows indexes where value exists
    for col in columnNames:
        rows = list(result[col][result[col] == True].index)
        for row in rows:
            listOfPos.append((row, col))
    # Return a list of tuples indicating the positions of value in the dataframe
    return listOfPos


# In[409]:


listOfPositions = getIndexes(data,'Figo')
listOfPositions


# In[410]:


m=listOfPositions[0][1]
m


# In[411]:


x=listOfPositions[0][0]
x


# In[412]:


data=data.iloc[x:,0:14]


# In[413]:


data.head()


# In[414]:


data.drop([data.columns[0]],axis=1,inplace=True)


# In[415]:


data.reset_index(inplace=True)


# In[416]:


data.drop([data.columns[0]],axis=1,inplace=True)


# In[417]:


data.head()


# In[418]:


data.rename(columns = {m:'Car Name', 'Unnamed: 2':1,'Unnamed: 3':2,'Unnamed: 4':3,
                      'Unnamed: 5':4,'Unnamed: 6':5,'Unnamed: 7':6,'Unnamed: 8':7,
                      'Unnamed: 9':8,'Unnamed: 10':9,'Unnamed: 11':10,'Unnamed: 12':11,'Unnamed: 13':12}, inplace = True)


# In[419]:


data.head()


# In[420]:


#Mapping Data


# In[421]:


df=pd.read_excel('gs://mmm-ford-de-bucket/data/mapping/Mapping v1.1.xlsx',sheet_name='Segment')


# In[422]:


df


# In[423]:


df.columns = df.iloc[0]
df= df[1:]
df.head()


# In[424]:


df.drop(['Car Name-new'],axis=1,inplace=True)


# In[425]:


data1=df.merge(data, how='inner', on='Car Name')


# In[426]:


data1.head()


# In[427]:


x = data1["Manufacturer"] +'@'+ data1["Car Name"]+'@'+data1["Segment"]+'@'+data1["Sub_Segment"]


# In[428]:


data1.insert(loc = 0,column = 'Name',value = x)


# In[429]:


data1.head()


# In[430]:


data1.drop(['Manufacturer','Car Name','Segment','Sub_Segment'],axis=1,inplace=True)


# In[431]:


data1.head()


# In[432]:


data1=data1.T


# In[433]:


data1.reset_index(inplace=True)


# In[434]:


data1.head()


# In[435]:


data1.columns = data1.iloc[0]
data1= data1[1:]
data1.head()


# In[436]:


data1.head()


# In[437]:


d=pd.melt(data1,id_vars=['Name'])


# In[438]:


d.head()


# In[439]:


new = d[0].str.split("@", n = 3, expand = True)
  
d["Manufacturer"]= new[0]
d["Car Name"]= new[1]
d['Segment']=new[2]
d['Sub_Segment']=new[3]
  
# Dropping old Name columns
d.drop(columns =[0], inplace = True)
  
# df display
d


# In[440]:


d.rename(columns = {'Name':'Month', 'value':'Sales'}, inplace = True)


# In[441]:


d.head()


# In[442]:


d.insert(0,'Year',yr)


# In[443]:


d.drop_duplicates(inplace=True)


# In[444]:


d.drop(d.index[d['Car Name'] =='Focus Total'], inplace=True)


# In[445]:


d.drop(d.index[d['Car Name'] =='F-Series SD'], inplace=True)


# In[446]:


#removing Premium and (blank) segment..


# In[447]:


y=d[(d['Segment']!='Premium')&(d['Segment']!='(blank)')]
y=y.fillna(0)
y.to_csv("gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2021_Sales.csv")


# In[448]:


x=d[(d['Segment']!='Premium')&(d['Segment']!='(blank)')]


# In[449]:


x.Segment.value_counts()


# In[450]:


x=x.fillna(0)


# In[451]:


x.max()


# In[452]:


x.columns


# In[453]:


x['Sales'].value_counts()


# In[454]:


x.dtypes


# In[455]:


x['Sales']=x['Sales'].astype('int64')


# In[456]:


x['Sales'].min()


# In[457]:


r=[i for i in range(2,x['Sales'].max()+1)]
s=[i for i in range(x['Sales'].min()-1,0)]


# In[458]:


x['Sales']=x['Sales'].replace(r,1)


# In[459]:


x['Sales']=x['Sales'].replace(s,0)


# In[460]:


x.Sales.value_counts()


# In[461]:


x.Segment.value_counts()


# In[462]:


#for Ford


# In[463]:


sku_Ford=x[x['Manufacturer']=='Ford']


# In[464]:


sku_Ford.head()


# In[465]:


sku_Ford['Sales'].sum()


# In[466]:


z=sku_Ford.groupby(sku_Ford["Month"]).Sales.agg(["sum"])
                                           


# In[467]:


z.reset_index(inplace=True)


# In[468]:


z.insert(0,'Year',yr)


# In[469]:


z.rename(columns={'sum':'Ford_sku_count'},inplace=True)


# In[470]:


z


# In[471]:


#for comp


# In[472]:


sku_Comp=x[x['Manufacturer']!='Ford']


# In[473]:


sku_Comp.head()


# In[474]:


sku_Comp['Sales'].sum()


# In[475]:


w=sku_Comp.groupby(sku_Comp["Month"]).Sales.agg(["sum"])


# In[476]:


w.reset_index(inplace=True)


# In[477]:


w.insert(0,'Year',yr)


# In[478]:


w.rename(columns={'sum':'Comp_sku_count'},inplace=True)


# In[479]:


w


# In[480]:


sku1=pd.concat([z,w],axis=1)
sku1 = sku1.T.drop_duplicates().T
sku1


# In[481]:


sku1.to_csv("gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2021_sku_count.csv")


# In[ ]:





# # Concat Data

# In[482]:


import pandas as pd
from google.cloud import storage
client = storage.Client()
import numpy as np


# In[483]:


d1=pd.read_csv('gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2016_sku_count.csv')


# In[484]:


d2=pd.read_csv('gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2017_sku_count.csv')


# In[485]:


d3=pd.read_csv('gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2018_sku_count.csv')


# In[486]:


d4=pd.read_csv('gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2019_sku_count.csv')


# In[487]:


d5=pd.read_csv('gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2020_sku_count.csv')


# In[488]:


d6=pd.read_csv('gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2021_sku_count.csv')


# In[489]:


d=pd.concat([d1,d2,d3,d4,d5,d6],axis=0)


# In[490]:


d.drop(['Unnamed: 0'],axis=1,inplace=True)


# In[491]:


d.Year.value_counts()


# In[492]:


d.reset_index(inplace=True)


# In[493]:


d.drop(['index'],axis=1,inplace=True)


# In[494]:


d.to_csv("gs://mmm-ford-de-bucket/output/SalesData/sku_count.csv")


# In[495]:


d.head(24)


# In[496]:


f1=pd.read_csv('gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2016_Sales.csv')


# In[497]:


f2=pd.read_csv('gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2017_Sales.csv')


# In[498]:


f3=pd.read_csv('gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2018_Sales.csv')


# In[499]:


f4=pd.read_csv('gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2019_Sales.csv')


# In[500]:


f5=pd.read_csv('gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2020_Sales.csv')


# In[501]:


f6=pd.read_csv('gs://mmm-ford-de-bucket/output/SalesDataYearlyBasis/2021_Sales.csv')


# In[502]:


f=pd.concat([f1,f2,f3,f4,f5,f6],axis=0)


# In[503]:


f.drop(['Unnamed: 0'],axis=1,inplace=True)


# In[504]:


f.reset_index(inplace=True)


# In[505]:


f.drop(['index'],axis=1,inplace=True)


# In[506]:


f.to_csv("gs://mmm-ford-de-bucket/output/SalesData/Sales.csv")


# In[507]:


f

